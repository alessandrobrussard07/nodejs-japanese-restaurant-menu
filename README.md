
# React JS Learning Project: Japanese Restaurant Menù

### What is this project?

This app created by Alessandro Brussard to learn the usage of ReactJS Javascript Framework.

The app simulates the interactive menu of a Japanese restaurant, in which there are dishes that the user can remove or rearrange at will.



### Inside this project:

This project contains the default ReactJS-app files, and the customizations created by me.

### How to install:

### (NODEJS MUST HAVE BEEN INSTALLED)


1. Clone this Repo.
1. Start a new CMD.
1. Navigate to the main directory of the project.
1. type <b>npm </b><b>start </b>to start the development server.

<i>A </i><i>new </i><i>browser </i><i>tab </i><i>will </i><i>pop-up </i><i>displaying </i><i>the </i><i>app.</i>



### ChangeLog:

New updates will be listed here.

