// this is a component in ReactJS
function Navbar() {
    return(
        //JSX Sintax
        <nav className="navbar bg-dark navbar-expand-lg bg-body-tertiary" data-bs-theme="dark">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Estia Japanese Restaurant</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <a className="nav-link active" aria-current="page" href="#">Home</a>
                <a className="nav-link" href="#">Page</a>
                <a className="nav-link" href="#">Other Page</a>
                <a className="nav-link disabled" aria-disabled="true">By Alessandro Brussard</a>
              </div>
            </div>
          </div>
        </nav>  
    );
}
// you need to ALWAYS export the component
export default Navbar;