/* 
you need to import all the components that you want to use
in this section
*/
import Navbar from "./components/navbar";
import Card from "./components/card";
import React, { Component } from "react";

import california from "./images/california.png";
import dragon from "./images/dragon.png";
import dynamite from "./images/dynamite.png";
import philadelphia from "./images/philadelphia.png";
import rainbow from "./images/rainbow.png";
import shrimp from "./images/shrimp.png";

// this is the main component
class App extends Component{
  state = {
    cards: [
      {id: 0, immagine: california, nome: "California", prezzo: 1.99},
      {id: 1, immagine: dragon, nome: "Dragon", prezzo: 2.99},
      {id: 2, immagine: dynamite, nome: "Dynamite", prezzo: 1.99},
      {id: 3, immagine: philadelphia, nome: "Philadelphia", prezzo: 2.49},
      {id: 4, immagine: dragon, nome: "Dragon", prezzo: 1.99},
      {id: 5, immagine: dynamite, nome: "Dynamite", prezzo: 1.99},
    ]
  }

  //Remove buttons 
  handleDelete = cardId => {
    const cards = this.state.cards.filter(card => card.id !== cardId);
    this.setState({ cards });
  }

  render() {
    return (
      //start of JSX code (jsx fragment)
      <> 
      <Navbar /> {/* call of imported component*/}
      <div className="container">
        <h1>Interactive Menù  </h1>
        <br />
        
        <div className="row">
          {this.state.cards.map(card => (
            <Card
              key={card.id}
              nome={card.nome}
              prezzo={card.prezzo}
              immagine={card.immagine} 
              onDelete = {this.handleDelete}/>
          ))}
        </div>

      </div>
         
      </>
      //end of JSX code
    );
  }
}
//You need to export the component to be able to display it on the page
export default App;
